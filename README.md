# Laravel Redbean ORM

*A Redbean ORM bridge for Laravel 5*

## Installation

Require this package  

```php
composer require laravel-redbean/orm
```

After adding the package, add the ServiceProvider to the providers array in `config/app.php`

```php
LaravelRedbean\ORM\RedbeanServiceProvider::class,
```

To publish the config use:

```php
php artisan vendor:publish
```

## Usage

By default, Redbean will use the default connection in `config/database.php`.

You can now use Redbean as normal. Please read the [Redbean documentation](http://www.redbeanphp.com).

## Multiple Connections

You can use multiple database connections by setting `multiple_connections => true,` in `config/redbean.php` which will then attempt to add each of `connections` in `config/database.php`.

By passing in the connection name from `connections` to `RedBeanPHP\R::selectDatabase('connection_name');` you will get the connection you want. So for example, if you wanted to connect to `mysql`:

```php
'connections' => [

        'sqlite' => [
            'driver'   => 'sqlite',
            'database' => database_path('database.sqlite'),
            'prefix'   => '',
        ],

        'mysql' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST', 'localhost'),
            'database'  => env('DB_DATABASE', 'forge'),
            'username'  => env('DB_USERNAME', 'forge'),
            'password'  => env('DB_PASSWORD', ''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

    ],
```

Select `mysql` connection:

```php
RedBeanPHP\R::selectDatabase('mysql');
```

## Authentication

The package provides a default User model (`LaravelRedbean\ORM\User::class`) you can use or create your own based on it.

### Implementing Authenticatable

Your user model must extend `RedBeanPHP\SimpleModel` and implement the `Illuminate\Contracts\Auth\Authenticatable` contract.

You may also use the provided trait `LaravelRedbean\ORM\Auth\Authenticatable` in your model and override where necessary.

### Configuring Laravel

Edit Laravel's Auth configuration (`/config/auth.php`) to use with Redbean.

Set the users provider to the following:

```php
'users' => [
    'driver' => 'redbean',
    'model' => LaravelRedbean\ORM\User::class,
],
```

## Password resets

Your user model must implement the `Illuminate\Contracts\Auth\CanResetPassword` contract.

You may also use the provided trait `LaravelRedbean\ORM\Auth\Passwords\CanResetPassword` in your model and override where necessary.