<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Fluid and Frozen
    |--------------------------------------------------------------------------
    |
    | When set to false, RedBeanPHP will keep changing the schema to fit your
    | needs, this is what we call 'fluid mode'. While this is great for
    | development you don't want this to happen on your production
    | server. That's why you need to freeze the schema before
    | you deploy your application. To freeze your app,
    | set to 'frozen' => true
    |
    */
    'frozen'                => env('REDBEAN_FROZEN', true),
    
    /*
    |--------------------------------------------------------------------------
    | Connections
    |--------------------------------------------------------------------------
    |
    | When set to false, RedBeanPHP will add one database connection. Setting
    | to true will add all database connections in database.php config.
    |
    */
    'multiple_connections'  => env('REDBEAN_MULTI', false),
];